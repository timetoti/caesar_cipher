﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Caesar_cipher.Tests
{
    [TestClass]
    public class DocxWorkerTests
    {
        [TestMethod]
        public void ReadText_test1()
        {
            string filename = AppDomain.CurrentDomain.BaseDirectory.Remove(AppDomain.CurrentDomain.BaseDirectory.Length - 9, 9)+"test.docx";
            string expected = "Some text for reading.\nIt’s awesome!\n";

            string actual = DocxWorker.ReadText(filename);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WriteText_test1()
        {
            string filename = AppDomain.CurrentDomain.BaseDirectory.Remove(AppDomain.CurrentDomain.BaseDirectory.Length - 9, 9) + "new.docx";
            string towrite = "Some text for reading.\nIt’s awesome!\n";
            byte[] expected = File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory.Remove(AppDomain.CurrentDomain.BaseDirectory.Length - 9, 9) + "test.docx");

            DocxWorker.WriteText(filename, towrite);
            byte[] actual = File.ReadAllBytes(filename);
            bool check = true;
            using (BinaryReader br1 = new BinaryReader(new FileStream(filename, FileMode.Open)))
            {
                using (BinaryReader br2 = new BinaryReader(new FileStream(AppDomain.CurrentDomain.BaseDirectory.Remove(AppDomain.CurrentDomain.BaseDirectory.Length - 9, 9) + "test.docx", FileMode.Open)))
                {
                    byte b1 = br1.ReadByte();
                    byte b2 = br2.ReadByte();
                    if (b1 != b2)
                    {
                        check = false;
                    }
                }
            }

            File.Delete(filename);

            Assert.AreEqual(true, check);
        }
    }
}
