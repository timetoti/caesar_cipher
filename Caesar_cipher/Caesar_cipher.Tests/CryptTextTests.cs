﻿using Caesar_cipher;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Caesar_cipher.Tests
{
    [TestClass]
    public class CryptTextTests
    {
        [TestMethod]
        public void Decrypt_LowerCaseShiftForward()
        {
            //arrange
            string input = "абя";
            int shift = 1;
            string expected = "яаю";

            //act
            string actual = CryptText.Decrypt(input, shift);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Decrypt_UpperCaseShiftForward()
        {
            string input = "АБЯ";
            int shift = 1;
            string expected = "ЯАЮ";

            string actual = CryptText.Decrypt(input, shift);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Decrypt_LowerCaseBackwardShift()
        {
            //arrange
            string input = "абя";
            int shift = -1;
            string expected = "бва";

            //act
            string actual = CryptText.Decrypt(input, shift);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Decrypt_UpperCaseBackwardShift()
        {
            string input = "АБЯ";
            int shift = -1;
            string expected = "БВА";

            string actual = CryptText.Decrypt(input, shift);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Decrypt_Otherchars()
        {
            string input = @"0Text1@,.;'/\";
            int shift = 2;
            string expected = @"0Text1@,.;'/\";

            string actual = CryptText.Decrypt(input, shift);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Encrypt_LowerCaseShiftForward()
        {
            //arrange
            string input = "абя";
            int shift = 1;
            string expected = "бва";

            //act
            string actual = CryptText.Encrypt(input, shift);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Encrypt_UpperCaseShiftForward()
        {
            string input = "АБЯ";
            int shift = 1;
            string expected = "БВА";

            string actual = CryptText.Encrypt(input, shift);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Encrypt_LowerCaseBackwardShift()
        {
            //arrange
            string input = "абя";
            int shift = -1;
            string expected = "яаю";

            //act
            string actual = CryptText.Encrypt(input, shift);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Encrypt_UpperCaseBackwardShift()
        {
            string input = "АБЯ";
            int shift = -1;
            string expected = "ЯАЮ";

            string actual = CryptText.Encrypt(input, shift);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Encrypt_Otherchars()
        {
            string input = @"0Text1@,.;'/\";
            int shift = 2;
            string expected = @"0Text1@,.;'/\";

            string actual = CryptText.Encrypt(input, shift);

            Assert.AreEqual(expected, actual);
        }
    }
}
