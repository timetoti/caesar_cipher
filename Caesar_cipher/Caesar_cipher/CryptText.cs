﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caesar_cipher
{
    public static class CryptText
    {
        static Dictionary<int, char> alphr = new Dictionary<int, char>
        {
            {0, 'а' },
            {1, 'б' },
            {2, 'в' },
            {3,'г' },
            {4,'д' },
            {5,'е' },
            {6,'ё' },
            {7,'ж' },
            {8,'з' },
            {9,'и' },
            {10,'й' },
            {11,'к' },
            {12,'л' },
            {13,'м' },
            {14,'н' },
            {15,'о' },
            {16,'п' },
            {17,'р' },
            {18,'с' },
            {19,'т' },
            {20,'у' },
            {21,'ф' },
            {22,'х' },
            {23,'ц' },
            {24,'ч' },
            {25,'ш' },
            {26,'щ' },
            {27,'ъ' },
            {28,'ы' },
            {29,'ь' },
            {30,'э' },
            {31,'ю' },
            {32,'я' }
        };
        static Dictionary<char, int> alph = new Dictionary<char, int>
        {
            {'а',0 },
            {'б',1 },
            {'в',2 },
            {'г',3 },
            {'д',4 },
            {'е',5 },
            {'ё',6 },
            {'ж',7 },
            {'з',8 },
            {'и',9 },
            {'й',10 },
            {'к',11},
            {'л',12 },
            {'м',13 },
            {'н',14 },
            {'о',15 },
            {'п',16 },
            {'р',17 },
            {'с',18 },
            {'т',19 },
            {'у',20 },
            {'ф',21 },
            {'х',22 },
            {'ц',23 },
            {'ч',24 },
            {'ш',25 },
            {'щ',26 },
            {'ъ',27 },
            {'ы',28 },
            {'ь',29 },
            {'э',30 },
            {'ю',31 },
            {'я',32 }
        };
        static Dictionary<int, char> aLphr = new Dictionary<int, char>
            {
            {0, 'А' },
            {1, 'Б' },
            {2, 'В' },
            {3,'Г' },
            {4,'Д' },
            {5,'Е' },
            {6,'Ё' },
            {7,'Ж' },
            {8,'З' },
            {9,'И' },
            {10,'Й' },
            {11,'К' },
            {12,'Л' },
            {13,'М' },
            {14,'Н' },
            {15,'О' },
            {16,'П' },
            {17,'Р' },
            {18,'С' },
            {19,'Т' },
            {20,'У' },
            {21,'Ф' },
            {22,'Х' },
            {23,'Ц' },
            {24,'Ч' },
            {25,'Ш' },
            {26,'Щ' },
            {27,'Ъ' },
            {28,'Ы' },
            {29,'Ь' },
            {30,'Э' },
            {31,'Ю' },
            {32,'Я' }
        };
        static Dictionary<char, int> aLph = new Dictionary<char, int>
            {
            {'А',0 },
            {'Б',1 },
            {'В',2 },
            {'Г',3 },
            {'Д',4 },
            {'Е',5 },
            {'ё',6 },
            {'Ж',7 },
            {'З',8 },
            {'И',9 },
            {'Й',10 },
            {'К',11},
            {'Л',12 },
            {'М',13 },
            {'Н',14 },
            {'О',15 },
            {'П',16 },
            {'Р',17 },
            {'С',18 },
            {'Т',19 },
            {'У',20 },
            {'Ф',21 },
            {'Х',22 },
            {'Ц',23 },
            {'Ч',24 },
            {'Ш',25 },
            {'Щ',26 },
            {'Ъ',27 },
            {'Ы',28 },
            {'Ь',29 },
            {'Э',30 },
            {'Ю',31 },
            {'Я',32 }
        };

        public static string Decrypt(string input, int shift)
        {
            string output = "";

            for (int i=0; i< input.Length; i++)
            {
                if (alph.ContainsKey(input[i]))
                {
                    int code = alph[input[i]] - shift;
                    while (code < 0) { code += 33; }
                    output += alphr[code % 33];
                }
                else
                {
                    if (aLph.ContainsKey(input[i]))
                    {
                        int code = aLph[input[i]] - shift;
                        while (code < 0) { code += 33; }
                        output += aLphr[code % 33];
                    }
                    else
                    {
                        output += input[i];
                    }
                }
            }

            return output;
        }

        public static string Encrypt(string input, int shift)
        {
            string output = "";

            for (int i = 0; i < input.Length; i++)
            {
                if (alph.ContainsKey(input[i]))
                {
                    int code = alph[input[i]] + shift;
                    while (code < 0) { code += 33; }
                    output += alphr[code % 33];
                }
                else
                {
                    if (aLph.ContainsKey(input[i]))
                    {
                        int code = aLph[input[i]] + shift;
                        while (code < 0) { code += 33; }
                        output += aLphr[code % 33];
                    }
                    else
                    {
                        output += input[i];
                    }
                }
            }

            return output;
        }
    }
}
