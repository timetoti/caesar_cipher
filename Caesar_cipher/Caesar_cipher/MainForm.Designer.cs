﻿namespace Caesar_cipher
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ChosenLoadType = new System.Windows.Forms.ComboBox();
            this.InputText = new System.Windows.Forms.RichTextBox();
            this.OutputText = new System.Windows.Forms.RichTextBox();
            this.LabelInput = new System.Windows.Forms.Label();
            this.LabelOutput = new System.Windows.Forms.Label();
            this.LabelChooseFile = new System.Windows.Forms.Label();
            this.BtnLoadText = new System.Windows.Forms.Button();
            this.LabelChooseAction = new System.Windows.Forms.Label();
            this.ChosenAction = new System.Windows.Forms.ComboBox();
            this.LabelChooseParams = new System.Windows.Forms.Label();
            this.ChosenDirection = new System.Windows.Forms.ComboBox();
            this.ChosenShift = new System.Windows.Forms.NumericUpDown();
            this.BtnAction = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.ChosenSaveType = new System.Windows.Forms.ComboBox();
            this.SaveFileName = new System.Windows.Forms.TextBox();
            this.LabelSaveType = new System.Windows.Forms.Label();
            this.LabelChooseFolder = new System.Windows.Forms.Label();
            this.BtnSavingFolder = new System.Windows.Forms.Button();
            this.LabelChooseFileName = new System.Windows.Forms.Label();
            this.LabelSavePath = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ChosenShift)).BeginInit();
            this.SuspendLayout();
            // 
            // ChosenLoadType
            // 
            this.ChosenLoadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChosenLoadType.FormattingEnabled = true;
            this.ChosenLoadType.Items.AddRange(new object[] {
            "Ручной ввод текста",
            "Загрузка текста из .txt файла",
            "Загрузка текста из .docx файла"});
            this.ChosenLoadType.Location = new System.Drawing.Point(11, 58);
            this.ChosenLoadType.Name = "ChosenLoadType";
            this.ChosenLoadType.Size = new System.Drawing.Size(183, 21);
            this.ChosenLoadType.TabIndex = 0;
            this.ChosenLoadType.SelectedIndexChanged += new System.EventHandler(this.ChosenLoadType_SelectedIndexChanged);
            // 
            // InputText
            // 
            this.InputText.Location = new System.Drawing.Point(302, 42);
            this.InputText.Name = "InputText";
            this.InputText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.InputText.Size = new System.Drawing.Size(486, 183);
            this.InputText.TabIndex = 1;
            this.InputText.Text = "";
            this.InputText.Visible = false;
            // 
            // OutputText
            // 
            this.OutputText.Location = new System.Drawing.Point(302, 255);
            this.OutputText.Name = "OutputText";
            this.OutputText.ReadOnly = true;
            this.OutputText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.OutputText.Size = new System.Drawing.Size(486, 183);
            this.OutputText.TabIndex = 2;
            this.OutputText.Text = "";
            this.OutputText.Visible = false;
            // 
            // LabelInput
            // 
            this.LabelInput.AutoSize = true;
            this.LabelInput.Location = new System.Drawing.Point(299, 26);
            this.LabelInput.Name = "LabelInput";
            this.LabelInput.Size = new System.Drawing.Size(80, 13);
            this.LabelInput.TabIndex = 3;
            this.LabelInput.Text = "Входной текст";
            this.LabelInput.Visible = false;
            // 
            // LabelOutput
            // 
            this.LabelOutput.AutoSize = true;
            this.LabelOutput.Location = new System.Drawing.Point(299, 239);
            this.LabelOutput.Name = "LabelOutput";
            this.LabelOutput.Size = new System.Drawing.Size(88, 13);
            this.LabelOutput.TabIndex = 4;
            this.LabelOutput.Text = "Выходной текст";
            this.LabelOutput.Visible = false;
            // 
            // LabelChooseFile
            // 
            this.LabelChooseFile.AutoSize = true;
            this.LabelChooseFile.Location = new System.Drawing.Point(9, 42);
            this.LabelChooseFile.Name = "LabelChooseFile";
            this.LabelChooseFile.Size = new System.Drawing.Size(182, 13);
            this.LabelChooseFile.TabIndex = 5;
            this.LabelChooseFile.Text = "Выберите способ загрузки текста";
            // 
            // BtnLoadText
            // 
            this.BtnLoadText.Location = new System.Drawing.Point(200, 56);
            this.BtnLoadText.Name = "BtnLoadText";
            this.BtnLoadText.Size = new System.Drawing.Size(96, 23);
            this.BtnLoadText.TabIndex = 6;
            this.BtnLoadText.Text = "Загрузить";
            this.BtnLoadText.UseVisualStyleBackColor = true;
            this.BtnLoadText.Visible = false;
            this.BtnLoadText.Click += new System.EventHandler(this.BtnLoadText_Click);
            // 
            // LabelChooseAction
            // 
            this.LabelChooseAction.AutoSize = true;
            this.LabelChooseAction.Location = new System.Drawing.Point(9, 82);
            this.LabelChooseAction.Name = "LabelChooseAction";
            this.LabelChooseAction.Size = new System.Drawing.Size(152, 13);
            this.LabelChooseAction.TabIndex = 7;
            this.LabelChooseAction.Text = "Выберите действие к тексту";
            this.LabelChooseAction.Visible = false;
            // 
            // ChosenAction
            // 
            this.ChosenAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChosenAction.FormattingEnabled = true;
            this.ChosenAction.Items.AddRange(new object[] {
            "Расшифровать",
            "Зашифровать"});
            this.ChosenAction.Location = new System.Drawing.Point(8, 98);
            this.ChosenAction.Name = "ChosenAction";
            this.ChosenAction.Size = new System.Drawing.Size(186, 21);
            this.ChosenAction.TabIndex = 8;
            this.ChosenAction.Visible = false;
            this.ChosenAction.SelectedIndexChanged += new System.EventHandler(this.ChosenAction_SelectedIndexChanged);
            // 
            // LabelChooseParams
            // 
            this.LabelChooseParams.AutoSize = true;
            this.LabelChooseParams.Location = new System.Drawing.Point(9, 122);
            this.LabelChooseParams.Name = "LabelChooseParams";
            this.LabelChooseParams.Size = new System.Drawing.Size(189, 13);
            this.LabelChooseParams.TabIndex = 9;
            this.LabelChooseParams.Text = "Укажите шаг и направление шифра";
            this.LabelChooseParams.Visible = false;
            // 
            // ChosenDirection
            // 
            this.ChosenDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChosenDirection.FormattingEnabled = true;
            this.ChosenDirection.Items.AddRange(new object[] {
            "Влево",
            "Вправо"});
            this.ChosenDirection.Location = new System.Drawing.Point(73, 138);
            this.ChosenDirection.Name = "ChosenDirection";
            this.ChosenDirection.Size = new System.Drawing.Size(121, 21);
            this.ChosenDirection.TabIndex = 10;
            this.ChosenDirection.Visible = false;
            this.ChosenDirection.SelectedIndexChanged += new System.EventHandler(this.ChosenDirection_SelectedIndexChanged);
            // 
            // ChosenShift
            // 
            this.ChosenShift.Location = new System.Drawing.Point(12, 139);
            this.ChosenShift.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.ChosenShift.Name = "ChosenShift";
            this.ChosenShift.Size = new System.Drawing.Size(55, 20);
            this.ChosenShift.TabIndex = 11;
            this.ChosenShift.Visible = false;
            // 
            // BtnAction
            // 
            this.BtnAction.Location = new System.Drawing.Point(8, 165);
            this.BtnAction.Name = "BtnAction";
            this.BtnAction.Size = new System.Drawing.Size(288, 60);
            this.BtnAction.TabIndex = 12;
            this.BtnAction.Text = "Действие";
            this.BtnAction.UseVisualStyleBackColor = true;
            this.BtnAction.Visible = false;
            this.BtnAction.Click += new System.EventHandler(this.BtnAction_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(8, 378);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(288, 60);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "Сохранить";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Visible = false;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // ChosenSaveType
            // 
            this.ChosenSaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChosenSaveType.FormattingEnabled = true;
            this.ChosenSaveType.Items.AddRange(new object[] {
            "Сохранить в формате .txt",
            "Сохранить в формате .docx"});
            this.ChosenSaveType.Location = new System.Drawing.Point(8, 255);
            this.ChosenSaveType.Name = "ChosenSaveType";
            this.ChosenSaveType.Size = new System.Drawing.Size(219, 21);
            this.ChosenSaveType.TabIndex = 14;
            this.ChosenSaveType.Visible = false;
            this.ChosenSaveType.SelectedIndexChanged += new System.EventHandler(this.ChosenSaveType_SelectedIndexChanged);
            // 
            // SaveFileName
            // 
            this.SaveFileName.Location = new System.Drawing.Point(8, 337);
            this.SaveFileName.Name = "SaveFileName";
            this.SaveFileName.Size = new System.Drawing.Size(263, 20);
            this.SaveFileName.TabIndex = 15;
            this.SaveFileName.Visible = false;
            this.SaveFileName.TextChanged += new System.EventHandler(this.SaveFileName_TextChanged);
            // 
            // LabelSaveType
            // 
            this.LabelSaveType.AutoSize = true;
            this.LabelSaveType.Location = new System.Drawing.Point(5, 239);
            this.LabelSaveType.Name = "LabelSaveType";
            this.LabelSaveType.Size = new System.Drawing.Size(218, 13);
            this.LabelSaveType.TabIndex = 16;
            this.LabelSaveType.Text = "Выберите способ сохранения результата";
            this.LabelSaveType.Visible = false;
            // 
            // LabelChooseFolder
            // 
            this.LabelChooseFolder.AutoSize = true;
            this.LabelChooseFolder.Location = new System.Drawing.Point(5, 279);
            this.LabelChooseFolder.Name = "LabelChooseFolder";
            this.LabelChooseFolder.Size = new System.Drawing.Size(266, 13);
            this.LabelChooseFolder.TabIndex = 17;
            this.LabelChooseFolder.Text = "Выберите папку, в которую хотите сохранить файл";
            this.LabelChooseFolder.Visible = false;
            // 
            // BtnSavingFolder
            // 
            this.BtnSavingFolder.Location = new System.Drawing.Point(8, 295);
            this.BtnSavingFolder.Name = "BtnSavingFolder";
            this.BtnSavingFolder.Size = new System.Drawing.Size(153, 23);
            this.BtnSavingFolder.TabIndex = 18;
            this.BtnSavingFolder.Text = "Перейти к выбору папки";
            this.BtnSavingFolder.UseVisualStyleBackColor = true;
            this.BtnSavingFolder.Visible = false;
            this.BtnSavingFolder.Click += new System.EventHandler(this.BtnSavingFolder_Click);
            // 
            // LabelChooseFileName
            // 
            this.LabelChooseFileName.AutoSize = true;
            this.LabelChooseFileName.Location = new System.Drawing.Point(9, 321);
            this.LabelChooseFileName.Name = "LabelChooseFileName";
            this.LabelChooseFileName.Size = new System.Drawing.Size(135, 13);
            this.LabelChooseFileName.TabIndex = 19;
            this.LabelChooseFileName.Text = "Введите название файла";
            this.LabelChooseFileName.Visible = false;
            // 
            // LabelSavePath
            // 
            this.LabelSavePath.AutoSize = true;
            this.LabelSavePath.Location = new System.Drawing.Point(12, 362);
            this.LabelSavePath.Name = "LabelSavePath";
            this.LabelSavePath.Size = new System.Drawing.Size(0, 13);
            this.LabelSavePath.TabIndex = 20;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LabelSavePath);
            this.Controls.Add(this.LabelChooseFileName);
            this.Controls.Add(this.BtnSavingFolder);
            this.Controls.Add(this.LabelChooseFolder);
            this.Controls.Add(this.LabelSaveType);
            this.Controls.Add(this.SaveFileName);
            this.Controls.Add(this.ChosenSaveType);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnAction);
            this.Controls.Add(this.ChosenShift);
            this.Controls.Add(this.ChosenDirection);
            this.Controls.Add(this.LabelChooseParams);
            this.Controls.Add(this.ChosenAction);
            this.Controls.Add(this.LabelChooseAction);
            this.Controls.Add(this.BtnLoadText);
            this.Controls.Add(this.LabelChooseFile);
            this.Controls.Add(this.LabelOutput);
            this.Controls.Add(this.LabelInput);
            this.Controls.Add(this.OutputText);
            this.Controls.Add(this.InputText);
            this.Controls.Add(this.ChosenLoadType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Шифрование/дешифрование шифром Цезаря";
            ((System.ComponentModel.ISupportInitialize)(this.ChosenShift)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ChosenLoadType;
        private System.Windows.Forms.RichTextBox InputText;
        private System.Windows.Forms.RichTextBox OutputText;
        private System.Windows.Forms.Label LabelInput;
        private System.Windows.Forms.Label LabelOutput;
        private System.Windows.Forms.Label LabelChooseFile;
        private System.Windows.Forms.Button BtnLoadText;
        private System.Windows.Forms.Label LabelChooseAction;
        private System.Windows.Forms.ComboBox ChosenAction;
        private System.Windows.Forms.Label LabelChooseParams;
        private System.Windows.Forms.ComboBox ChosenDirection;
        private System.Windows.Forms.NumericUpDown ChosenShift;
        private System.Windows.Forms.Button BtnAction;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.ComboBox ChosenSaveType;
        private System.Windows.Forms.TextBox SaveFileName;
        private System.Windows.Forms.Label LabelSaveType;
        private System.Windows.Forms.Label LabelChooseFolder;
        private System.Windows.Forms.Button BtnSavingFolder;
        private System.Windows.Forms.Label LabelChooseFileName;
        private System.Windows.Forms.Label LabelSavePath;
    }
}

