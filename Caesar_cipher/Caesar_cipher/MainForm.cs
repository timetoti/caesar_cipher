﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Caesar_cipher
{
    public partial class MainForm : Form
    {
        private string loadfilename = "", savefolderpath = "", extension = "";

        public MainForm()
        {
            InitializeComponent();
        }

        private void MakeVis(params Control[] controls)
        {
            foreach(Control c in controls)
            {
                c.Visible = true;
            }
        }

        private void MakeInvis(params Control[] controls)
        {
            foreach (Control c in controls)
            {
                c.Visible = false;
            }
        }

        private void ChosenLoadType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChosenLoadType.SelectedIndex==0)
            {
                MakeVis(InputText, LabelInput, LabelChooseAction, ChosenAction, LabelChooseParams, ChosenShift, ChosenDirection);
                MakeInvis(BtnLoadText);
                LabelInput.Text = "Введите текст";
            }
            else
            {
                MakeVis(BtnLoadText);
            }
        }

        private void ChosenAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChosenAction.SelectedIndex >= 0 && ChosenDirection.SelectedIndex >= 0)
            {
                MakeVis(BtnAction);
            }
            else
            {
                MakeInvis(BtnAction);
            }
        }

        private void ChosenDirection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChosenAction.SelectedIndex >= 0 && ChosenDirection.SelectedIndex >= 0)
            {
                MakeVis(BtnAction);
            }
            else
            {
                MakeInvis(BtnAction);
            }
        }

        private void BtnAction_Click(object sender, EventArgs e)
        {
            if (InputText.Text == "")
            {
                MessageBox.Show("Исходный текст отсутствует!");
            }
            else
            {
                int shift = (int)ChosenShift.Value;
                if (ChosenDirection.SelectedIndex == 0)
                {
                    shift = -shift;
                }

                if (ChosenAction.SelectedIndex == 0)
                {
                    OutputText.Text = CryptText.Decrypt(InputText.Text, shift);
                }
                else
                {
                    OutputText.Text = CryptText.Encrypt(InputText.Text, shift);
                }
                MakeVis(LabelOutput, OutputText, LabelSaveType, ChosenSaveType);
            }
        }

        private void ChosenSaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeVis(LabelChooseFolder, BtnSavingFolder);
            if (ChosenSaveType.SelectedIndex == 0)
            {
                extension = ".txt";
            }
            else
            {
                extension = ".docx";
            }

            if (SaveFileName.BackColor != Color.IndianRed && SaveFileName.Text != "")
            {
                LabelSavePath.Text = savefolderpath + "\\" + SaveFileName.Text + extension;
            }
        }

        private void BtnSavingFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                savefolderpath = fbd.SelectedPath;
                LabelSavePath.Text = savefolderpath + "\\" + SaveFileName.Text + extension;
                MakeVis(LabelChooseFileName, SaveFileName);

                if (SaveFileName.BackColor!=Color.IndianRed && SaveFileName.Text!="")
                {
                    MakeVis(BtnSave);
                }
                else
                {
                    MakeInvis(BtnSave);
                }
            }
        }

        private void SaveFileName_TextChanged(object sender, EventArgs e)
        {
            if (CheckNameValidity(SaveFileName.Text) && SaveFileName.Text != "")
            {
                MakeVis(BtnSave);
                LabelSavePath.Text = savefolderpath + "\\" + SaveFileName.Text + extension;
                SaveFileName.BackColor = Color.White;
            }
            else
            {
                MakeInvis(BtnSave);
                SaveFileName.BackColor = Color.IndianRed;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChosenSaveType.SelectedIndex == 0)
                {
                    File.WriteAllText(LabelSavePath.Text, OutputText.Text);
                }
                else
                {
                    DocxWorker.WriteText(LabelSavePath.Text, OutputText.Text);
                }
                MessageBox.Show("Успешно");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool CheckNameValidity(string name)
        {
            bool valid = true;

            foreach (char c in name)
            {
                if (c=='\\' || c=='/' || c==':' || c=='*' || c=='?' || c=='\"' || c=='<' || c=='>' || c=='|')
                {
                    valid = false;
                    break;
                }
            }

            return valid;
        }

        private void BtnLoadText_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ChosenLoadType.SelectedIndex==1) { ofd.Filter = "Text files(*.txt) | *.txt"; } else { ofd.Filter = "Word files(*.docx) | *.docx"; }

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                loadfilename = ofd.FileName;
                MakeVis(LabelChooseAction, ChosenAction, LabelChooseParams, ChosenShift, ChosenDirection, LabelInput, InputText);
                LabelInput.Text = "Текст из файла";
                if (ChosenLoadType.SelectedIndex == 1) { InputText.Text = File.ReadAllText(loadfilename); } else { InputText.Text = DocxWorker.ReadText(loadfilename); }
            }
        }
    }
}
