﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Words.NET;

namespace Caesar_cipher
{
    public static class DocxWorker
    {
        public static string ReadText(string filename)
        {
            string output = "";

            using (DocX doc = DocX.Load(filename))
            {
                foreach (var p in doc.Paragraphs)
                {
                    output += p.Text + '\n';
                }
            }

            return output;
        }

        public static void WriteText(string filename, string towrite)
        {
            char[] sc = { '\n' };
            string[] paragraphs = towrite.Split(sc, StringSplitOptions.RemoveEmptyEntries);

            using (DocX doc = DocX.Create(filename))
            {
                for (int i = 0; i < paragraphs.Length; i++)
                {
                    doc.InsertParagraph(paragraphs[i]);
                }

                doc.Save();
            }
        }
    }
}
